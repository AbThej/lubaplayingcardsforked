//PlayingCards
//Luba Letunovskaya
//Forked by THAY LEE

#include <iostream>
#include <conio.h>

using namespace std;

//enums for rank&suit

enum Rank
{
	Two = 2, Three, Four, Five, Six, Seven,
	Eight, Nine, Ten, Jack, Queen, King, Ace
};

enum Suit
{
	Spades = 3, Hearts = 2, Clubs = 0, Diamonds = 1
};


//struct for card

struct Card
{
	Rank rank;
	Suit suit;
};

//prototype

void PrintCard(Card card);
Card Highcard(Card a, Card b);

int main()
{
	Card a;
	a.rank = Ace;
	a.suit = Spades;

	Card b;
	b.rank = Ace;
	b.suit = Hearts;

	PrintCard(a);
	PrintCard(b);


	if (a.rank == b.rank && a.suit == b.suit) 
	{
		cout << "They are the same card!";
	}
	else
	{
		Card high = Highcard(a, b);
		cout << "The high card is: "; PrintCard(high);
	}

	(void)_getch();
	return 0;

}

//Print card function

void PrintCard(Card card)
{
	cout << "The ";
	switch (card.rank)
	{
	case 2: cout << "Two"; break;
	case 3: cout << "Three"; break;
	case 4: cout << "Four"; break;
	case 5: cout << "Five"; break;
	case 6: cout << "Six"; break;
	case 7: cout << "Seven"; break;
	case 8: cout << "Eight"; break;
	case 9: cout << "Nine"; break;
	case 10: cout << "Ten"; break;
	case 11: cout << "Jack"; break;
	case 12: cout << "Queen"; break;
	case 13: cout << "King"; break;
	case 14: cout << "Ace"; break;
	default: break;
	}
	cout << " of ";
	switch (card.suit)
	{
	case 0: cout << "Clubs "; break;
	case 1: cout << "Diamonds "; break;
	case 2: cout << "Hearts "; break;
	case 3: cout << "Spades "; break;
	default: break;
	}
	cout << "\n\n";
}

//Highcard function
Card Highcard(Card a, Card b)
{
	if (a.rank > b.rank)
	{
		return a;
	}
	else if (a.rank < b.rank)
	{
		return b;
	}
	else if (a.rank == b.rank)
	{
		if (a.suit > b.suit)
		{
			return a;
		}
		else if (a.suit < b.suit) 
		{
			return b;
		}
		else if (a.suit == b.suit) 
		{
			return a;
		}
	}
}